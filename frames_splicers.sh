#!/bin/bash

####
#
# Frames_splicer 
#
# input 
#   folder with movies files 
#   (opt) number of frames to splice (default to 2)
#   
# output 
#   movie file containing number of frames sequencialy interleaves   
#   output 24 fps prores mov
#
# Dependencies
#   ffmpeg
#
#   gllm.ca 2020/02/02 
#
#
####


assign_arguments()      
## $1 = movie_folder,  $2 = (opt):frames to splices in
{
    echo ${FUNCNAME[0]}
    MOVIE_FOLDER_PATH="$1"
    MOVIE_FOLDER_NAME=`basename "$MOVIE_FOLDER_PATH"`
    if [ -z "$2" ]; then
        echo "NO FRAME_TO_SPLICE arg provided, -> 2"
        FRAMES_TO_SPLICE="2"
    elif [ "$2" -lt 1 ]; then
         echo "FRAME_TO_SPLICE arg must be positive, -> 2"
        FRAMES_TO_SPLICE="2"    
    else
        FRAMES_TO_SPLICE="$2"
    fi   
}

create_dot_folder()         
## create_temp_folder
{
    echo ${FUNCNAME[0]}
    cd "$MOVIE_FOLDER_PATH"
    mkdir -p ."$MOVIE_FOLDER_NAME"
    cd ."$MOVIE_FOLDER_NAME"
}

clean_dot_folder()          
## remove traces 
{
    echo ${FUNCNAME[0]}
    rm -rf "$WORKPATH"
}

get_movie_files()           
## parses movies files to array of path 
{
    echo ${FUNCNAME[0]}
    shopt -s  nullglob  ## no empty expansion file globing
    MOVIE_FILES=("$MOVIE_FOLDER_PATH"/*{mov,mp4,mxf,m4v,avi,gif})
}

convert_movies_to_png()     
## output movie to png ordered folder (src_(i++))
{
    echo ${FUNCNAME[0]}
    for ((i=0; i<${#MOVIE_FILES[@]}; i++)); do
        echo "movie to PNG :: ${MOVIE_FILES[$i]}"
        mkdir -p "$WORKPATH"/"src_$i"
        ffmpeg -hide_banner -v quiet -stats -i  "${MOVIE_FILES[$i]}" "$WORKPATH/src_$i"/image-%06d.png
    done
}

splice_rename_png()         
## rename png in src_X folder according to a index skipping compose of number count and frame_to_splice arg 
{ 
    echo ${FUNCNAME[0]}
    i=0;                                            ## Iterator for folder count (start_offset)
    MOVIES_NUM="${#MOVIE_FILES[@]}"                 
    for d in "$WORKPATH"/*/ ; do                    ## get the folders   
        cd "$d"                                     ## cd to the current folder                              
        ((START_OFFSET=i*$FRAMES_TO_SPLICE))        ## start offset of frame numbering
        ((i++))                                     ##  i++ready for the next loop
        FRAME_BUFFER=$FRAMES_TO_SPLICE
        ((FRAME_INDEX=$START_OFFSET))
        for j in "$d"/*.png; do
            if [ "$FRAME_BUFFER" -lt 1 ]; then      ## when frame buffer is empty jump to next index
                 ((FRAME_BUFFER=$FRAMES_TO_SPLICE-1))
                 ((FRAME_INDEX=1+$FRAME_INDEX+($MOVIES_NUM-1)*$FRAMES_TO_SPLICE))   ## next index math
            else                                    ## else increment index reduce buffer
                ((FRAME_INDEX++))                   
                ((FRAME_BUFFER--))  
            fi
            new=$(printf "%06d.png" "$FRAME_INDEX") ## create the name with padding zero
            mv -i -- "$j" "$new"                    ## rename / move 
        done
    done
}

concatenate_png()
## 
{
    echo ${FUNCNAME[0]}
    mkdir -p "$CONCAT_PATH"
    for d in "$WORKPATH"/*/ ; do
         for f in "$d"/*.png; do
            mv $f "$CONCAT_PATH"
         done
    done
}

rename_png_to_sequence()            
## help to trim uneven movie files by sequencialy renaming all the files
{
    echo ${FUNCNAME[0]}
    a=0
    for i in "$CONCAT_PATH"/*.png; do
        new=$(printf "%06d.png" "$a")
        mv -i -- "$i" "$CONCAT_PATH"/"$new"
        let a=a+1
    done
}

create_movie_file_from_sequence()   
## create prores file (todo; implement a argument passing with presets)
{
    echo ${FUNCNAME[0]}
    ffmpeg \
    -hide_banner \
    -v quiet -stats \
    -f image2 \
    -framerate 24 \
    -i "$CONCAT_PATH"/"%06d.png" \
    -c:v prores_ks \
    -profile:v 3 \
    "$MOVIE_FILES_PATH"
}

move_movie_to_root_folder() 
## get the movie aside from the originating movie folder
{
    echo ${FUNCNAME[0]}
    cd $MOVIE_FOLDER_PATH
    cd ..
    mv "$MOVIE_FILES_PATH" "./"
}

### MAIN 
assign_arguments "$@"
WORKPATH="$MOVIE_FOLDER_PATH"/."$MOVIE_FOLDER_NAME/"
clean_dot_folder
create_dot_folder 
get_movie_files
convert_movies_to_png
splice_rename_png
CONCAT_PATH="$WORKPATH"/.out 
concatenate_png
rename_png_to_sequence
MOVIE_FILES_PATH="$CONCAT_PATH"/"$MOVIE_FOLDER_NAME"_"$FRAMES_TO_SPLICE"_f.mov  ##file name w/ splice count
create_movie_file_from_sequence
move_movie_to_root_folder
clean_dot_folder
