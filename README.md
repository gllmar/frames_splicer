# frames_splicer

bash script to automate frame splicing from movie folder to one movie file

## dependencies 

* ffmpeg
* bash 

## How to use:

* prepare the movie folder
    * populate with as many movie files you want to interleave toguether 
        * supported type a
    * folder name will become the movie name
    * 
* open a terminal
* drag drop `frames_splicers.sh` in the terminal window
* drag drop the movie folder you want after 


### input : 

movie folder 

### ouput : 

movie file in Prores HQ

